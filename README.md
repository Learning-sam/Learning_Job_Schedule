# Learning_Job_Schedule

#### 项目介绍
任务调度：Java 应用任务调度框架

#### 软件架构
1. 操作系统定时任务
2. JDK 定时任务
3. Spring Schedule 
3. Quartz
4. TBSchedule
5. Elastic Job
6. Oozie （DAG）\SchedulerX\Saturn\Lhotse

#### 分类
1. DAG：oozie，azkaban，chronos，zeus，Lhotse
2. 定时分片类系统：TBSchedule，SchedulerX，Elastic-job, Saturn

#### 项目结构
1. System_Job_Schedule  系统定时任务 `linux：crontab; windex：计划任务`
2. JDK_Job_Schedule JDK 定时任务 `Timer; ScheduledExecutor`
3. Spring Schedule Spring 定时任务
4. Quartz 任务调度
5. Elastic Job 当当开源
6. TBSchedule 淘宝开源

#### 基于 zk 的 Quartz 简单分布式调度思路
1. 保证任务的唯一执行：Job 启动的时候，在zk新建Job唯一标识的临时节点
2. 监控节点信息，Job临时节点删除后，监控到的应用节点启动该Job任务