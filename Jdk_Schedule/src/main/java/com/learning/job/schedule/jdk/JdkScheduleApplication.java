package com.learning.job.schedule.jdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdkScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(JdkScheduleApplication.class, args);
    }

}

