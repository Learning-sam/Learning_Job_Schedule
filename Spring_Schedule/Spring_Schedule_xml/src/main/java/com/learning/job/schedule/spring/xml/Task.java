package com.learning.job.schedule.spring.xml;

/**
 * ClassName: Task
 * Description:
 * Date: 2018/12/17 15:20 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class Task {

    public void doTask() {
        System.out.println("~~~ doTask 定时任务被执行 ~~~");
    }

    public void doTaskFixedDelay() {
        System.out.println("~~~ doTaskFixedDelay 定时任务被执行 ~~~");
    }
}
