package com.learning.job.schedule.quartz.base.simple;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * ClassName: SimpleJob
 * Description:
 * Date: 2018/12/18 10:56 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class SimpleJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        JobDetail jobDetail = context.getJobDetail();

        // 在Job执行时，JobExecutionContext中的JobDataMap为我们提供了很多的便利。
        // 它是 JobDetail 中的 JobDataMap 和 Trigger 中的 JobDataMap 的并集，但是如果存在相同的数据，则后者会覆盖前者的值。
        JobDataMap jobDataMap = context.getMergedJobDataMap();
        String value = jobDataMap.getString("key");

        System.out.println("~~ SimpleJob 执行 ~~ " + value);
    }
}
