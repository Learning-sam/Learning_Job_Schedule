package com.learning.job.schedule.quartz.spring.xml;

/**
 * ClassName: MyMethodJob
 * Description: 可以实现 Job 接口
 * Date: 2018/12/18 10:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class MyMethodJob {

    public void execute() {
        System.out.println("MyJob");
    }
}
