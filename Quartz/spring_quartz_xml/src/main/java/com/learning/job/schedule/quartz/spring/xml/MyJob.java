package com.learning.job.schedule.quartz.spring.xml;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * ClassName: MyJob
 * Description:
 * Date: 2018/12/18 10:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class MyJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("MyJob");
    }
}
