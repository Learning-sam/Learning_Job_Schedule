package com.learning.job.schedule.quartz.springboot;

import org.springframework.stereotype.Component;

/**
 * ClassName: MyBootJob
 * Description:
 * Date: 2018/12/18 16:24 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Component
public class MyBootMethodJob {

    public void exe() {
        System.out.println("~~ MyBootMethodJob ~~");
    }
}
