package com.learning.job.schedule.springboot.dynamic.job;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * ClassName: DemoJob
 * Description:
 * Date: 2019/5/9 13:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@DisallowConcurrentExecution
public class DemoJob extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        System.out.println("~~ DemoJob 启动运行汇总~~");
    }
}