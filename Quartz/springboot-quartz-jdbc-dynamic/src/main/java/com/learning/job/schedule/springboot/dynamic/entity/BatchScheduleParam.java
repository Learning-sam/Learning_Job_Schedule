package com.learning.job.schedule.springboot.dynamic.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: BatchScheduleParam
 * Description: 任务计划参数
 * Date: 2018/9/11 9:32 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
public class BatchScheduleParam {

    /**
     * 任务计划ID
     */
    private String scheduleId;

    /**
     * 任务计划code
     */
    private String scheduleCode;

    /**
     * 参数名
     */
    private String paramName;

    /**
     * 参数值
     */
    private String paramValue;

    /**
     * 动态参数使用
     */
    private String tempId;

}
