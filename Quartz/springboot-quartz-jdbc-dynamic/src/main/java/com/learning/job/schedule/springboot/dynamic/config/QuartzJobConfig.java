package com.learning.job.schedule.springboot.dynamic.config;

import com.learning.job.schedule.springboot.dynamic.job.DemoJob;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * ClassName: QuartzJobConfig
 * Description:
 * Date: 2019/5/9 13:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
public class QuartzJobConfig {


    @Bean
    public JobDetailFactoryBean jobDetailFactoryBean() {
        JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
        jobDetail.setName("DemoJob");
        jobDetail.setGroup("DemoJob_Group");
        jobDetail.setJobClass(DemoJob.class);
        jobDetail.setDurability(true);
        return jobDetail;
    }

    /**
     * 表达式触发器工厂Bean
     */
    @Bean
    public CronTriggerFactoryBean cronTriggerFactoryBean() {
        CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
        trigger.setJobDetail(jobDetailFactoryBean().getObject());
        trigger.setCronExpression("*/10 * * * * ?");
        trigger.setName("DemoJob");
        trigger.setMisfireInstruction(0);

        return trigger;
    }

}
