package com.learning.job.schedule.springboot.dynamic.config;

import com.learning.job.schedule.springboot.dynamic.listener.CustomGlobalJobListener;
import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * ClassName: SchedulerFactoryBeanCustomizerConfig
 * Description: SchedulerFactoryBean 自定义配置
 * Date: 2019/4/24 16:07 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
public class SchedulerFactoryBeanCustomizerConfig implements SchedulerFactoryBeanCustomizer {

    /**
     * 全局监听器
     *
     * @return
     */
    @Bean
    public CustomGlobalJobListener globalJobListener() {
        return new CustomGlobalJobListener();
    }


    @Override
    public void customize(SchedulerFactoryBean schedulerFactoryBean) {
        schedulerFactoryBean.setGlobalJobListeners(globalJobListener());
    }
}
