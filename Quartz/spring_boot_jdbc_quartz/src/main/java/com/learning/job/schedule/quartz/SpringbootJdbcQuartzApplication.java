package com.learning.job.schedule.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJdbcQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJdbcQuartzApplication.class, args);
    }

}

