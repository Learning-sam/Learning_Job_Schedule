package com.learning.job.schedule.quartz.jdbc;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * ClassName: JdbcJob
 * Description:
 * Date: 2018/12/18 16:54 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Component
public class JdbcJob extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        System.out.println("~~ JdbcJob ~~");

        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {

        }
    }
}
