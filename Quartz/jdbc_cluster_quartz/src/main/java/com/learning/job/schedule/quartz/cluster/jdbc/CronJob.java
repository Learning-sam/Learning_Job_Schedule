package com.learning.job.schedule.quartz.cluster.jdbc;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * ClassName: CronJob
 * Description:
 * Date: 2018/12/18 13:41 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class CronJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("~~ CronJob ~~");
    }
}
