package com.example.base_elastic_job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseElasticJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseElasticJobApplication.class, args);
    }

}

