package com.example.base_elastic_job;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseElasticJobApplicationTests {

    @Test
    public void contextLoads() {
    }

}

